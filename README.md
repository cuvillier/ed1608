ED1608 parser 

This project contains the Javascript (node.js) code to parse the payload coming from the ED1681 Lora sensor from 1M2M.

To setup:
-------------------------------------------------------------
Install node.js
Run the test script runtest.bat

How to use it ?
-------------------------------------------------------------
...
var ed1608 = require('ED1608.js');
var json = ed1608.parse('0102096100064f7a3c07a50300000000');

json contains:
{
	"MsgIdInt" : 1,
	"MsgIdString" : "Tracking",
	"DevType" : "4221455",
	"Start" : 0,
	"Move" : 1,
	"Stop" : 0,
	"Vibr" : 0,
	"Temp" : 24.01,
	"FixAge" : 0,
	"SatInFix" : 6,
	"Latitude" : 52.08636,
	"Longitude" : 5.00995
}
...

Test data
-------------------------------------------------------------
The test data are stored in src/test/ED1608_Test_Data.json and contains:
The hex-encoded payload from the sensor.
The resulting JSon.

The payload decoding is cheked from the 1m2m online decoding service:
http://1m2m.eu/services/GETPAYLOAD?Human=0&PL=0200f35904bd247d1e00000000000000


Versions
-------------------------------------------------------------

Version 1:
- Initial commit. Only Alive and Trakcing messages are supported


Any question or suggestion? cuvillier@gmail.com
