
/*
 * 1m2m ED1608 Lora sensor payload parsers.
 * 
 * (C)? no, Open Source
 * 
 * Thibault Cuvillier
 * imad-ge.ch 11.2017
 */

/**
 * Parse a the ED1608 payload encoded in hexa.
 * 1 btye = 2 char in the payload.
 * 
 * Example: byte 10 = "0A"
 * Big endian.
 */
class Payload {
	constructor(payloadHex) {
		this.payloadHex = payloadHex;
		this.buffer = new Buffer(payloadHex, 'hex');
		this.here = 0;
	}

	getUnsignedInt(nbytes) {
		var value = this.buffer.readUIntBE(this.here, nbytes);
		this.here += nbytes;
		return value;
	}

	getInt(nbytes) {
		var value = this.buffer.readIntBE(this.here, nbytes);
		this.here += nbytes;
		return value;
	}

	getByte() {
		var value = this.buffer.readUInt8(this.here);
		this.here++;
		return value;
	}

	getByteArray(nbytes) {
		var bytes = [];
		for(var i = 0; i < nbytes; i++) {
			bytes[i] = this.buffer.readUInt8(this.here);
			this.here++;
		}

		return bytes;
	}

	getBit(bitfield, ibit, nbits) {
		var value = 0;
		var pow = 1;
		for(var i = 0; i < nbits; i++) {
			var bit = (bitfield >> (ibit + i)) % 2
			value += bit * pow;
			pow *= 2;
		}

		return value;
	}
}

/**
 * Base class used by the ED1608 messages parsers
 */
class ED1608PayloadParser {

	constructor(msgIdInt, msgIdString) {
		this.MsgIdInt = msgIdInt;
		this.MsgIdString = msgIdString;
		this.DevType = "4221455";
	}

	mapToFiware(json) {
		throw "This method should be overrided, fix the code!";
	}

	parse(payloadHex) {
		var payload = new Payload(payloadHex);

		var json = {};
		this.copyTo(payload, json);
		return json;
	}

	copyTo(payload, json) {
		var plMsgIdInt = payload.getByte();

		if(plMsgIdInt != this.MsgIdInt)
			throw 'Invalid Payload type ' + plMsgIdInt +': not a ' + this.MsgIdInt;

		json.MsgIdInt = this.MsgIdInt;
		json.MsgIdString = this.MsgIdString;
		json.DevType = this.DevType;
	}

	parseGPSFixAge(byte) {
	/* 
	 * Pascal code:
	  if r.GPSFixAge<60   then FixAge:=r.GPSFixAge              else
	  if r.GPSFixAge<120  then FixAge:=60+(r.GPSFixAge-60)*5    else
	  if r.GPSFixAge<255  then FixAge:=360+(r.GPSFixAge-120)*30 else FixAge:=0;
	  if r.GPSFixAge<255  then writeln('FixAge='+timetostrx(-FixAge/minperday))
	                      else writeln('Fixage=""');
	 */	
		var fixAge;
		if(byte < 60 ) 
			return byte;
		if(byte < 120) return 60 + (byte - 60) * 5;

		return 360 + (byte - 120) * 30;
	}

	decbin(dec,length) {
		  var out = "";
		  while(length--)
		    out += (dec >> length ) & 1;    
		  return out;  
	}

	parseSatInFix(satCntHillByte) {

		// writeln('SatInFix='+IntToStr(r.SatCnt_HiLL and $1F));
		var satInFix = satCntHillByte & 0x1F;
		return satInFix;
	}

	parseLatitude(bytes, satCntHillByte) {
		/*
		  Lat := (r.Lat[0] shl 16) or (r.Lat[1] shl 8) or (r.Lat[2]);
		  if r.SatCnt_HiLL and $20>0 then lat:=lat or $FF000000;
		 */
		var latitude = (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
/*
		if( (satCntHillByte & 0x20) > 0) latitude |= 0xFF000000;
*/
		return latitude / 100000.0;
	}

	parseLongitude(bytes, satCntHillByte) {
		/*
		Lon := (r.Lon[0] shl 16) or (r.Lon[1] shl 8) or (r.Lon[2]);
		if r.SatCnt_HiLL and $40>0 then lon:=lon or $01000000;
		if r.SatCnt_HiLL and $80>0 then lon:=lon or $FE000000;
		 */
		var longitude = (bytes[0] << 16) | (bytes[1] << 8) | bytes[2];
/*
		if( satCntHillByte & 0x40 > 0) longitude |= 0x01000000;
		if( satCntHillByte & 0x80 > 0) longitude |= 0xFE000000;	
	*/	
		return longitude / 100000.0;
	}

	parseTemp(tempUnsignedInt16) {
		return tempUnsignedInt16 / 100;
	}
	
	parseBattery(batteryByte) {
		return batteryByte * 100 / 256;
	}

	parseBaromBar(int16) {
		return (100000 + int16);
	}

	stringConst(s) {
		return '"' + s + "'";
	}
}

/**
 * Parse the ED1608 Alive Message
 */
class PayloadParserAlive extends ED1608PayloadParser {
	constructor() {
		super(0x00, "Alive");
	}

	copyTo(payload, json) {
		super.copyTo(payload, json);
	
		json.Battery = this.parseBattery(payload.getByte());
	
		var bitfield = payload.getUnsignedInt(1);
		json.DigIn1 = payload.getBit(bitfield, 0);
		json.DigIn2 = payload.getBit(bitfield, 1);
		json.Spare1 = payload.getBit(bitfield, 2);
		json.Spare2 = payload.getBit(bitfield, 3);
		json.Spare3 = payload.getBit(bitfield, 4);
		json.Spare4 = payload.getBit(bitfield, 5);
		json.Spare5 = payload.getBit(bitfield, 6);
		json.PowerOut = payload.getBit(bitfield, 7);
		json.CmdAck = payload.getUnsignedInt(1);
		json.FixAge = this.parseGPSFixAge(payload.getByte());

		var satCntHillByte = payload.getByte();
		json.SatInFix = this.parseSatInFix(satCntHillByte);

		json.Latitude = this.parseLatitude(payload.getByteArray(3), satCntHillByte);
		json.Longitude = this.parseLongitude(payload.getByteArray(3), satCntHillByte);
		
		return json;
	}

	mapToFiware(date, json) {
		return [
			{
				type: "Device???",
			    batteryCapacity:json.Battery
			},
		    {
				type: "Bicycle",
		        location: {
	                type: "Point",
	                coordinates:[json.Latitude, json.Longitude]
	            }
		    }
	    ];
	}
}

/**
 * Parse the ED1608 Tracking Message
 */
class PayloadParserTracking extends ED1608PayloadParser {

	constructor() {
		super(0x01, "Tracking");
	}

	copyTo(payload, json) {
		super.copyTo(payload, json);

		var bitfield = payload.getByte();
		json.Start = payload.getBit(bitfield, 0, 1);
		json.Move = payload.getBit(bitfield, 1, 1);
		json.Stop = payload.getBit(bitfield, 2, 1);
		json.Vibr = payload.getBit(bitfield, 3, 1);

		json.Temp = this.parseTemp(payload.getUnsignedInt(2));
		json.FixAge = payload.getByte();

		var satCntHillByte = payload.getByte();
		json.SatInFix = this.parseSatInFix(satCntHillByte);
		json.Latitude = this.parseLatitude(payload.getByteArray(3), satCntHillByte);
		json.Longitude = this.parseLongitude(payload.getByteArray(3), satCntHillByte);
	
		return json;
	}

	mapToFiware(date, json) {
		var woID = "ED1608-0x02-" + date;
		return [
		    {
		    	id:"????",
				type: "Bicycle",
				source:"www.imad-ge.ch",
				dateObserved:date,
		    	temperature:json.Temp,
		        location: {
	                type: "Point",
	                coordinates:[json.Latitude, json.Longitude]
	            }
		    },

			{
		    	id:woID,
				type: "WeatherObserved",
				source:"www.imad-ge.ch",
				dateObserved:date,
		    	temperature:json.Temp,
		        location: {
	                type: "Point",
	                coordinates:[json.Latitude, json.Longitude]
	            }
		    }
	    ];
	}
}

/**
 * Parse the ED1608 GenSens Message
 */
class PayloadParserGenSens extends ED1608PayloadParser {

	constructor() {
		super(0x02, "GenSens");
	}

	copyTo(payload, json) {
		super.copyTo(payload, json);

		json.Status = payload.getByte();
		json.BaromBar = this.parseBaromBar(payload.getInt(2));
		json.Temp = this.parseTemp(payload.getUnsignedInt(2));
		json.Humidity = payload.getByte();

		json.LevelX = payload.getInt(1);
		json.LevelY = payload.getInt(1);
		json.LevelZ = payload.getInt(1);

		json.VibAmp = payload.getUnsignedInt(1);
		json.VibFreq= payload.getUnsignedInt(1);

		return json;
	}

	mapToFiware(date, json) {
		var woID = "ED1608-0x02-" + date;
		return [
		    {
		    	id:woID,
				type: "WeatherObserved",
				source:"www.imad-ge.ch",
				dateObserved:date,
		    	temperature:json.Temp,
		    	relativeHumidity:json.Humidity/100,
		        location: {
	                type: "Point",
	                coordinates:[json.Latitude, json.Longitude]
	            },
		        barometricPressure:json.BaromBar
		    }
	    ];
	}
}

/**
 * Parse the ED1608 Rotation Message
 */
class PayloadParserRotation extends ED1608PayloadParser {

	constructor() {
		super(0x03, "Rotation");
	}

	copyTo(payload, json) {
		super.copyTo(payload, json);

		var bitfield = payload.getByte(2);
		json.GravRot = payload.getBit(bitfield, 0, 1);
		json.MagRot = payload.getBit(bitfield, 1, 1);

		json.GravX = payload.getInt(1);
		json.GravY = payload.getInt(1);
		json.GravZ = payload.getInt(1);

		json.MagX = payload.getInt(1);
		json.MagY = payload.getInt(1);
		json.MagZ = payload.getInt(1);
		
		return json;
	}
}

/**
 * Parse the ED1608 Alarm Message
 */
class PayloadParserAlarm extends ED1608PayloadParser {

	constructor() {
		super(0x04, "Alarm");
	}

	copyTo(payload, json) {
		super.copyTo(payload, json);

		var bitfield = payload.getByte();
		json.GravRot = payload.getBit(bitfield, 0, 1);
		json.MagRot = payload.getBit(bitfield, 1, 1);
		json.MotAl = payload.getBit(bitfield, 2, 1);
		json.GeofenceAl = payload.getBit(bitfield, 3, 1);
		json.VibrAl = payload.getBit(bitfield, 4, 1);
		json.ShockAl = payload.getBit(bitfield, 5, 1);
		json.DigIn1Al = payload.getBit(bitfield, 6, 1);
		json.DigIn2Al = payload.getBit(bitfield, 7, 1);

		json.Temp = parseTemp(payload.getInt(1));
		json.Hum = payload.getInt(1);
		json.BaromBar = parseBaromBar(payload.getUnsignedInt(1));
		
		return json;
	}
}

/**
 * Main entry class exported by the module
 */
class ED1608 {
	constructor() {
		this.parsers = {};
		this.addPayloadParser(new PayloadParserAlive());
		this.addPayloadParser(new PayloadParserTracking());
		this.addPayloadParser(new PayloadParserGenSens());
		this.addPayloadParser(new PayloadParserRotation());
		this.addPayloadParser(new PayloadParserAlarm());
	}

	addPayloadParser(parser) {
		this.parsers[parser.MsgIdInt] = parser;
	}

	parse(payloadHex) {
		var msgIdInt = parseInt('0x'+ payloadHex.substring(0, 2));
		var parser = this.parsers[msgIdInt];
		if( !parser )
			return null;
//			throw 'Payload ' + msgIdInt + ' not found';

		return parser.parse(payloadHex);
	}

	test(payloadHex, expected) {
		var computed = this.parse(payloadHex);

		console.log(computed);
		// console.log(expected);

		Object.keys(expected).forEach(function(key,index) {

			if( !computed.hasOwnProperty(key)) {
				throw 'Property ' + key + ' is missing';
			}

			if( computed[key] != expected[key] ) {
				throw key + '=' + computed[key] + ' instead of ' + expected[key];
			}
			
			delete expected[key];
		});
	}

	/**
	 * Map the JSON parsed form the payload to a JSON compatible with the Fiware datamodel.
	 * http://fiware-datamodels.readthedocs.io/en/latest/index.html
	 * Multiple Json may result from the mapping.
	 * 
	 * The result is an array of json.
	 */
	mapToFiware(date, json) {
		var parser = this.parsers[json.MsgIdInt];
		return parser.mapToFiware(date, json);
	}
}

/**
 * Export module entry points.
 */
var ed1608Singleton = new ED1608()

exports.parse = (payloadHex) => ed1608Singleton.parse(payloadHex);
exports.test = (payloadHex, expected) => ed1608Singleton.test(payloadHex, expected);
exports.mapToFiware = (date, json) => ed1608Singleton.mapToFiware(date, json);
