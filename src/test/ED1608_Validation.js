
var ed1608 = require('../main/ED1608.js');

console.log('ED1608 Payload decoder test');

fs = require('fs');
fs.readFile('src/test/ED1608_Validation.json', 'utf8', function (err, fileContent) {

		if (err) {
	    return console.log(err);
	  }

	  console.log("payload;Date;MsgId;Battery;Temp;Humidity;SatInFix;Latitude;Longitudeude");
	  var data = JSON.parse(fileContent);
	  for(var dataIndex in data) {
		  var payloadHex = data[dataIndex].payload;
	  	  var json = ed1608.parse(payloadHex);
	  	  
	  	  if(json == null) continue;

	  	  var sdate = data[dataIndex].date;
	  	  var date = Date.parse(sdate);

	  	  switch(json.MsgIdInt) {
	  	  case 0:
	  		  console.log(json);

	  		  if(json.Battery >0) {
		  		  if( json.SatInFix == 0)
		  			  console.log(payloadHex + ";" + date +";" + json.MsgIdInt + ";"+ json.Battery+";;;0;;" + json.DigIn1);
		  		  else
		  			  console.log(payloadHex + ";" + date +";" + json.MsgIdInt + ";"+ json.Battery+";;;"+json.SatInFix+";"+json.Latitude + ";"+json.Longitude + ";" + json.DigIn1);
	  		  }

	  		  break;

	  	  case 1:

	  		  if( json.SatInFix == 0)
	  			  console.log(payloadHex + ";" + date +";" + json.MsgIdInt+";;"+ json.Temp+";;0;;");
	  		  else
	  			  console.log(payloadHex + ";" + date +";" + json.MsgIdInt+";;"+ json.Temp+";;"+json.SatInFix+";"+json.Latitude + ";"+json.Longitude);
	  		  break;

	  	  case 2:
		  	  console.log(payloadHex + ";" + date +";" + json.MsgIdInt+";;"+ json.Temp+";"+json.Humidity+";;;");
	  		  break;
	  	  }

	  	  var fiware = ed1608.mapToFiware(date, json);
	  	  console.log(fiware);
	  }
	});
