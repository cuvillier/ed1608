
var ed1608 = require('../main/ED1608.js');

console.log('ED1608 Payload decoder test');

fs = require('fs');
fs.readFile('src/test/ED1608_Test_Data.json', 'utf8', function (err, fileContent) {

	if (err) {
	    return console.log(err);
	  }

	  var json = JSON.parse(fileContent);

	  for(var jsonIndex in json) {
		  var testData = json[jsonIndex];
		  console.log('Testing payload ' + jsonIndex + ' = ' + testData.payload);
	  	  ed1608.test(testData.payload, testData.expected);
	  }
	});
